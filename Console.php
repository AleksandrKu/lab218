<?php

class Console
{
public static $success = "SUCCESS";
public static $failure = "FAILURE";
public static $warning = "WARNING";
public static $note = "NOTE";
public static function printLine($message, $status = '')
{
	$message = (string) $message;
		switch ($status) {
		case (self::$success):
			$prefix = "Success: ";
			$color = "[0;32m";
			break;
		case (self::$warning):
			$prefix = "Warning: ";
			$color = "[1;33m";
			break;
		case (self::$failure):
			$prefix = "Failue: ";
			$color = "[0;31m";
			break;
		case (self::$note):
			$color = "[0;34m";
			break;
		default:
			$prefix = "";
			$color = '[1;37m';
	}
	echo chr(27). $color . $prefix . $message .chr(27) . "[0m" . PHP_EOL;
}


}
