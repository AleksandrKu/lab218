<?php
class  Asus extends Computer
{
	const IS_DESKTOP = true;

	public function __construct()
	{
		$this->setCpu('Intel Core i3-4005U (1.7 Ghz)');
		$this->setRam('6 Gb');
		$this->setVideo('nVidia GeForce GT 920M');
		$this->setMemory('HDD 1 Tb');
		$this->setComputerName('Asus X540LJ');
	}

	public function identifyUser()
	{
		echo PHP_EOL.$this->getComputerName() . ': Identify by password' . PHP_EOL;
	}
}