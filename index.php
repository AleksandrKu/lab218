<?php
require_once(__DIR__ . '/Console.php');
require_once(__DIR__ . '/IComputer.php');
require_once(__DIR__ . '/Computer.php');
require_once(__DIR__ . '/Asus.php');
require_once(__DIR__ . '/Lenovo.php');
require_once(__DIR__ . '/Macbook.php');

$asus = new Asus();
$asus->restart2();
$asus->printParameters();
$asus->identifyUser();
echo PHP_EOL;

$lenovo = new Lenovo();
$lenovo->start();
$lenovo->printParameters();
$lenovo->identifyUser();
echo PHP_EOL;

$macbook = new MacBook();
$macbook->start();
$macbook->printParameters();
$macbook->identifyUser();
echo PHP_EOL;






/*if ($computer instanceof Asus) {
	$computer->shutDown();
	sleep(1);
	$computer->shutDown();
	sleep(1);
	$computer->restart2();
	sleep(1);
	$computer->start();
	sleep(1);
	$computer->restart2();
	sleep(1);

}*/